import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:ucsa_client/constants/config.dart';
import 'package:ucsa_client/widgets/spinner_widget.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:http/http.dart' as http;

class MyCustomWebViewWidget extends StatelessWidget {
  MyCustomWebViewWidget();
  final Completer<WebViewController> _controller =
      Completer<WebViewController>();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      // render content via the builder
      builder: (context, snapshot) {
        // render content if data is available
        if (snapshot.hasData) {
          // render a webview on the body
          return WebView(
            // initialUrl should always be given, can't be omitted
            initialUrl: '${Config.frontEnd}',
            javascriptMode: JavascriptMode.unrestricted,
            onWebViewCreated: (WebViewController webViewController) async {
              final headers = await getLoginData();
              final user = headers["username"];
              final token = headers["token"];
              var url =
                  '${Config.frontEnd}/#/login?username=${user}&token=${token}';
              await webViewController.loadUrl(url, headers: headers);
              // finally signal a complete() to the completer
              _controller.complete(webViewController);
            },
            javascriptChannels: <JavascriptChannel>{
              JavascriptChannel(
                  name: 'loginFlutterHandler',
                  onMessageReceived: (JavascriptMessage message) {
                    saveLoginData(message);
                  }),
              JavascriptChannel(
                  name: 'saveKeyValue',
                  onMessageReceived: (JavascriptMessage message) {
                    saveKeyValue(message);
                  }),
            },
          );
        }
        // show a spinner until the data is available
        return SpinnerWidget();
      },
      // source of data for the builder
      future: getBaseURI(),
    );
  }

  Future<http.Response> getBaseURI() async {
    var url = '${Config.frontEnd}';
    try {
      final res = await http.get(url);
      if (res.statusCode == 200) {
        return res;
      }
    } catch (err) {
      print(err);
    }
    print("returning empty item");
    return http.get('');
  }

  saveLoginData(JavascriptMessage message) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString("json_credentials", message.message);
  }

  Future<Map<String, String>> getLoginData() async {
    final prefs = await SharedPreferences.getInstance();
    var jsonString = prefs.getString("json_credentials");
    if (jsonString == 'undefined') {
      jsonString = '{"username": "", "token": "" }';
    }
    final map = json.decode(jsonString);
    final Map<String, String> settings = {
      "username": map["username"],
      "token": map["token"]
    };
    return settings;
  }

  saveKeyValue(JavascriptMessage message) async {
    final prefs = await SharedPreferences.getInstance();
    try {
      Map<String, String> content = jsonDecode(message.message);
      prefs.setString(content["key"], content["value"]);
    } catch (e) {
      print('Formato Json incorrecto. Esperaba "{"key": "", "value": ""}"');
    }
  }

  Future<String> getValueFromKey(JavascriptMessage message) async {
    final prefs = await SharedPreferences.getInstance();
    try {
      var contenido = prefs.getString(message.message);
      if (contenido == null) {
        contenido = "";
      }
      print(contenido);
      return contenido;
    } catch (e) {
      print('Formato Json incorrecto. Esperaba "{"key": ""}"');
      return "undefined";
    }
  }
}
