import {
  createRouter,
  createWebHashHistory,
  LocationQueryValue,
} from "vue-router";
import { backendCollector } from "../data/collectors/backendCollector";

export const router = createRouter({
  history: createWebHashHistory(),
  routes: [
    {
      path: "/",
      component: () => import("../components/Home.vue"),
    },
    {
      path: "/login",
      redirect: (to) => {
        console.log(to.fullPath);
        console.log(to.query.username);
        console.log(to.query.token);
        let user = to.query.username ?? "";
        let token = to.query.token ?? "";
        // @ts-ignore
        backendCollector.server.username = user;
        // @ts-ignore
        backendCollector.server.token = token;
        // to.query = {};
        return "/loginForm";
      },
    },
    {
      path: "/loginForm",
      component: () => import("../components/Login.vue"),
    },
  ],
});
