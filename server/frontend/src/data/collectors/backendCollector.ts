import { Chat } from "../models/Chat";
import { ChatRoom } from "../models/ChatRoom";
import { successData } from "../models/successData";
import { ClientProvider } from "../providers/ClientProvider";
import { Niveles, ServerProvider } from "../providers/ServerProvider";

class BackendCollector {
  server: ServerProvider;
  client: ClientProvider;

  constructor() {
    this.client = new ClientProvider();
    this.server = new ServerProvider();
  }
  async setup() {
    let { username, token } = await this.client.getToken();
    this.server.username = username;
    this.server.token = token;
    return this;
  }
  async login(): Promise<successData<{ nivel: Niveles }>> {
    const login = await this.server.getLogin();
    if (login.success) {
      this.client.saveNivel(login.data!.nivel);
      this.client.saveToken({
        username: this.server.username!,
        token: this.server.token!,
      });
    }
    return login;
  }
  async getChatList(): Promise<successData<Chat[]>> {
    const response = await this.server.getChatList();
    return response;
  }
  async getChatRoom(chat: Chat): Promise<successData<ChatRoom>> {
    return this.server.getChatRoom(chat);
  }
  async getChatsWithNewMessages(
    chatIds: ChatRoom["chatId"][]
  ): Promise<
    successData<Map<ChatRoom["chatId"], ChatRoom["mensajes"][number]>>
  > {
    return await this.server.getLastMensajes(chatIds);
  }
  async markAsRead(chatId: ChatRoom["chatId"]) {
    this.server.markAsRead(chatId);
  }
}

export const backendCollector = await new BackendCollector().setup();
