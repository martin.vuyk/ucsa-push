import { ApiResponse, LoginPost } from "../models/ApiResponse";
import { Chat } from "../models/Chat";
import { ChatRoom } from "../models/ChatRoom";
import { MateriaDict } from "../models/Materia";
import { successData } from "../models/successData";
type mapMsj = Map<ChatRoom["chatId"], ChatRoom["mensajes"][number]>;

export type Niveles = "usuario" | "profesor" | "administrador";
export class ServerProvider {
  backendUrl = `${import.meta.env.VITE_BACKEND_BASE_URL}/api/v1`;
  username: string | null = null;
  token: string | null = null;
  nivel: Niveles = "usuario";

  constructor() {}
  async getChatList(): Promise<successData<Chat[]>> {
    const url = `${this.backendUrl}/chatlist`;
    return this.server("GET", url);
  }
  async getChatRoom(chat: Chat): Promise<successData<ChatRoom>> {
    const url = `${this.backendUrl}/chatroom/${chat.id}`;
    return await this.server("GET", url);
  }
  async getLogin(): Promise<successData<{ nivel: Niveles }>> {
    const url = `${this.backendUrl}/login`;
    return await this.server<{ nivel: Niveles }>("GET", url);
  }
  async postLogin(
    username: string,
    password: string
  ): Promise<successData<LoginPost>> {
    const url = `${this.backendUrl}/login`;
    const body = { USERNAME: username, HASHED_PW: password };
    return await this.server("POST", url, body);
  }
  async postMensaje(
    materiaId: ChatRoom["chatId"],
    mensaje: ChatRoom["mensajes"][number]
  ): Promise<successData<null>> {
    const url = `${this.backendUrl}/producir`;
    const body = { mensajes: [mensaje], uid_materias: [materiaId] };
    return await this.server<null>("POST", url, body);
  }
  async multicastMensaje(
    materiaIds: ChatRoom["chatId"][],
    mensaje: ChatRoom["mensajes"][number]
  ) {
    const url = `${this.backendUrl}/producir`;
    const body = { mensajes: [mensaje], uid_materias: materiaIds };
    return await this.server<null>("POST", url, body);
  }
  async getMateriasRange(
    dias: number[],
    turnos: number[],
    profesores: string[]
  ) {
    const url = `${this.backendUrl}/materias?${new URLSearchParams({
      dias: JSON.stringify(dias),
      turnos: JSON.stringify(turnos),
      profesores: JSON.stringify(profesores),
    })}`;
    return await this.server<MateriaDict[]>("GET", url);
  }
  async getLastMensajes(
    chatIds: ChatRoom["chatId"][]
  ): Promise<successData<mapMsj>> {
    const url = `${this.backendUrl}/producir?${new URLSearchParams({
      ids: JSON.stringify(chatIds),
    })}`;
    const fromServer = await this.server<Object>("GET", url);
    const parsedMap: mapMsj = new Map(Object.entries(fromServer.data ?? {}));
    return {
      success: fromServer.success,
      code: fromServer.code,
      error: fromServer.error,
      data: parsedMap,
    };
  }
  async markAsRead(chatid: ChatRoom["chatId"]) {
    const url = `${this.backendUrl}/chatroom/${chatid}`;
    return await this.server<null>("PUT", url);
  }
  private async server<T>(
    method: "GET" | "POST" | "PUT" | "DELETE",
    url: string,
    body: any = null
  ): Promise<successData<T>> {
    const headers = {
      USERNAME: this.username ?? "",
      TOKEN: this.token ?? "",
      "Content-Type": "application/json",
    };
    try {
      let options: RequestInit = { method: method, headers: headers };
      if (body != null && body != undefined) {
        options = { ...options, body: JSON.stringify(body) };
      }
      const fetched = await fetch(url, options);
      const response: ApiResponse<T> = await fetched.json();
      return {
        success: response.success,
        data: response.data ?? null,
        code: response.code,
        error: response.error ?? null,
      };
    } catch (e) {
      return {
        success: false,
        data: null,
        code: 500,
        error: String(e),
      };
    }
  }
}

// async  pruebaGetChatList(): Promise<successData<Chat[]>> {
//   let prueba = {
//     id: "123",
//     icon: "mdi-school",
//     titulo: "chat de prueba",
//     descripcion: "chat hecho para probar las cosas y sus detalles",
//     previewMessage: "prueba",
//     lastMessageTime: "",
//   };
//   return await {
//     success: true,
//     data: [prueba, prueba, prueba, prueba, prueba, prueba],
//     code: 200,
//     error: null,
//   };
// }
// async function pruebaGetChatRoom(chat: Chat): Promise<successData<ChatRoom>> {
//   let mensaje1: ChatRoom["mensajes"][number] = {
//     emisor: "nombreEmisor",
//     titulo: "mensaje de prueba",
//     timestamp: "2022-12-22T15:23:13.596Z",
//     contenido: `Lorem ipsum dolor sit amet,
//     consectetur adipiscing elit. Aenean in malesuada velit.
//     In eget lacus volutpat, lobortis augue in, scelerisque diam.
//     Curabitur commodo pretium gravida. Fusce non ultricies dui.
//     Vestibulum ullamcorper lacus nec blandit sagittis.
//     Vestibulum at nulla dictum, aliquam dui vitae, tempus metus.
//     Quisque fringilla erat elit, vel luctus diam iaculis et.
//     Etiam lacus tortor, accumsan a nisl a, sagittis vestibulum nulla.
//     Lorem ipsum dolor sit amet, consectetur adipiscing elit.
//     Vivamus mollis pulvinar elit, non dictum urna sagittis quis. Vivamus eget est arcu`,
//     prioridad: "normal",
//   };
//   let mensaje2 = { ...mensaje1 };
//   mensaje2.prioridad = "alta";
//   let mensaje3 = { ...mensaje1 };
//   mensaje3.prioridad = "emergencia";
//   return await {
//     success: true,
//     data: {
//       chatId: "chatPrueba",
//       titulo: "Chat de Prueba",
//       descripcion: "Chat hecho para probar las cosas",
//       mensajes: [mensaje1, mensaje2, mensaje3],
//     },
//     code: 200,
//     error: null,
//   };
// }
