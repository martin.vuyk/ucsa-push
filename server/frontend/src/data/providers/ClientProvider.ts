import { Chat } from "../models/Chat";
import { ChatRoom } from "../models/ChatRoom";
import { successData } from "../models/successData";
type mapMsj = Map<ChatRoom["chatId"], ChatRoom["mensajes"][number]>;
export class ClientProvider {
  constructor() {}
  async saveNivel(nivel: string) {
    localStorage.setItem("nivel", nivel);
  }
  async saveToken(message: { username: string; token: string }) {
    const parsed = JSON.stringify(message);
    localStorage.setItem("json_credentials", parsed);
    try {
      eval(`loginFlutterHandler.postMessage('${parsed}')`);
    } catch (e) {
      console.log(`no se pudo mandar el mensaje a Flutter: \n${e}`);
    }
  }
  async getToken(): Promise<{ username: string; token: string }> {
    // no funciona
    // try {
    //   const info: string = eval(
    //     `getValueFromKey.postMessage("json_credentials");`
    //   );
    //   console.log(`Conseguido de Flutter: ${info}`);
    //   const loginData: { username: string; token: string } = JSON.parse(info);
    //   return loginData;
    // } catch (e) {
    //   console.log(`no se pudo recuperar el token de Flutter: ${e}`);
    // }
    const emptyMssg = { username: "", token: "" };
    const item = localStorage.getItem("json_credentials");
    if (item == null) return emptyMssg;
    return JSON.parse(item);

  }
}
