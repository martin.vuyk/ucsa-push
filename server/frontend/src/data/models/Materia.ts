export type MateriaDict = {
  uid: string;
  username_profesor: string;
  descripcion: string;
  dia_clase: number;
  turno: number;
  fecha_vencimiento: string;
};
