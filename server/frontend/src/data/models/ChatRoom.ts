export type ChatRoom = {
  chatId: string;
  titulo: string;
  descripcion: string;
  mensajes: {
    emisor: string;
    titulo: string;
    timestamp: string;
    contenido: string;
    prioridad: "normal" | "alta" | "emergencia";
  }[];
};
