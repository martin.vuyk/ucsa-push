export type ApiResponse<T> = {
  success: boolean;
  data?: T;
  error?: string;
  code: number;
};

export type LoginPost = {
  token: string;
};
