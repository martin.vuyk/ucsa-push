export type successData<T> = {
  success: boolean;
  data: T | null;
  code: number;
  error: string | null;
};
