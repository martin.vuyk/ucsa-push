export type Chat = {
  id: string;
  icon: string;
  previewMessage: string;
  titulo: string;
  descripcion: string;
  lastMessageTime: string;
};
