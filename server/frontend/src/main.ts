import { createApp } from "vue";
import App from "./App.vue";
import { router } from "./router";
import vuetify from "./plugins/vuetify";
import PerfectScrollbar from "vue3-perfect-scrollbar";
import "vue3-perfect-scrollbar/dist/vue3-perfect-scrollbar.css";

createApp(App).use(router).use(vuetify).use(PerfectScrollbar).mount("#app");
