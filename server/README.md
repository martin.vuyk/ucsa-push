## Arquitectura:

- ./backend: API Django Rest que administra las suscripciones y usuarios<br />
- ./frontend: Single Page Application que presenta la vista con la que el usuario comun interactua con la API <br />

### Deploy:

-descargar la carpeta<br />
-poner un archivo .env dentro con los siguientes campos:<br />
SQL_ENGINE=django.db.backends.postgresql<br />
SQL_DATABASE=<br />
SQL_USER=<br />
SQL_PASSWORD=<br />
SQL_HOST=db<br />
SQL_PORT=5432<br />
BACKEND_HOST=IPv4_addr<br />
BACKEND_PORT=4595<br />
VITE_BACKEND_BASE_URL="http://${BACKEND_HOST}:${BACKEND_PORT}"<br />
-escribir en una terminal: "docker compose up"

#### Requerimientos:

-docker
