from typing import List

from app.models import User, Materia, Mensaje, ChatRoom
from app.data_models.mensaje import MensajeDict


class Hermes:
    @classmethod
    def enviar_mensajes(
        cls,
        user: User,
        topics: List[str],
        mensajes: List[MensajeDict],
        unicast: bool = False,
    ):
        if unicast:
            for index, topic in enumerate(topics):
                actual = mensajes[index]
                # enviar_mensaje_sync(user, topic, mensajes[index if unicast else 0])
                materia = Materia.objects.get(uid=topic)
                mensaje = Mensaje.objects.create(
                    emisor=user,
                    titulo=actual["titulo"],
                    timestamp=actual["timestamp"],
                    contenido=actual["contenido"],
                    prioridad=actual["prioridad"],
                    materia=materia,
                )
                cls.__guardar_mensaje(materia, mensaje)
                # cls.__enviar_mensaje_whatsapp(materia, mensaje)

        else:
            for topic in topics:
                materia = Materia.objects.get(uid=topic)
                mensaje = Mensaje.objects.create(
                    emisor=user,
                    titulo=mensajes[0]["titulo"],
                    timestamp=mensajes[0]["timestamp"],
                    contenido=mensajes[0]["contenido"],
                    prioridad=mensajes[0]["prioridad"],
                    materia=materia,
                )
                cls.__guardar_mensaje(materia, mensaje)
                # cls.__enviar_mensaje_whatsapp(materia, mensaje)

    @staticmethod
    def __enviar_mensaje_whatsapp(materia: Materia, mensaje: Mensaje):
        # TODO
        pass

    @staticmethod
    def __guardar_mensaje(materia: Materia, mensaje: Mensaje):
        chatRoom, creado = ChatRoom.objects.update_or_create(
            materia=materia,
            defaults=dict(
                titulo=materia.descripcion,
                last_mensaje_time=mensaje.timestamp,
            ),
        )
        if creado:
            data = materia.get_object_data()
            chatRoom.update(icono="mdi-school", mensaje_vista=data["username_profesor"])
        chatRoom.mensajes.add(mensaje)
