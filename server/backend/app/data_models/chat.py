from typing import Dict, List, TypedDict, TypeVar
from typing_extensions import TypeGuard
from app.helpers.try_bool import try_false_type_guard


class ChatDict(TypedDict):
    id: str
    icon: str
    previewMessage: str
    titulo: str
    descripcion: str
    lastMessageTime: str


T = TypeVar("T")


class ChatRoomDict(TypedDict):
    chatId: str
    titulo: str
    descripcion: str
    mensajes: List[T]


@try_false_type_guard
def es_chat_room(thing) -> TypeGuard[ChatRoomDict]:
    if isinstance(thing, dict):
        return (
            isinstance(thing.get("chatId"), str)
            and isinstance(thing.get("titulo"), str)
            and isinstance(thing.get("descripcion"), str)
            and isinstance(thing.get("mensajes"), list)
        )


@try_false_type_guard
def es_chat(thing) -> TypeGuard[ChatDict]:
    if isinstance(thing, dict):
        return (
            isinstance(thing.get("id"), str)
            and isinstance(thing.get("titulo"), str)
            and isinstance(thing.get("descripcion"), str)
            and isinstance(thing.get("previewMessage"), str)
            and isinstance(thing.get("lastMessageTime"), str)
            and thing.get("icon") in ["mdi-school"]
        )
