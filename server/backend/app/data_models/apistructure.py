"""
Este archivo se encarga de dar estructura y mejorar el reciclaje de codigo en toda la API.
"""
from dataclasses import asdict, dataclass, fields
from django.db import models
from abc import abstractmethod
from typing import Any, Callable, Iterator, Dict, Literal, Optional, Union
from rest_framework.views import APIView
from django.http import JsonResponse


class APIModelStructure:
    """Estructura de los modelos en la DB"""

    @staticmethod
    def is_stored_in_DB(cls: models.Model, field: str, value: Any) -> bool:
        return cls.objects.filter(**{field: value}).exists()

    @abstractmethod
    def get_object_data() -> Dict:
        pass

    @staticmethod
    def get_objects_data(objects: Iterator["APIModelStructure"]):
        return [object.get_object_data() for object in objects]


@dataclass
class ApiResponse:
    """El formato de respuesta de los metodos http en la API"""

    success: bool
    code: int
    data: Optional[Any] = None
    error: Optional[str] = None


class ApiViewMetaClass(type):
    """MetaClass para incorporar la capacidad de aplicar wrappers a metodos
    sin tener que estar escribiendo uno a uno en el codigo"""

    def __new__(self, name, bases, attrs):
        """si la clase tiene metodo post, get, put o delete, wrappearle con @handle_http_errors"""
        methods = ["post", "get", "put", "delete"]
        for metodo in methods:
            if metodo in attrs:
                attrs[metodo] = self.handle_http_errors(attrs[metodo])
        return super(ApiViewMetaClass, self).__new__(self, name, bases, attrs)

    @staticmethod
    def handle_http_errors(fn: Callable) -> Callable:
        """Wrapper que ejecuta el codigo de cada request y se encarga de lidiar con las excepciones que
        va lenvantando el codigo"""

        def new_fn(request, *args, **kwargs):
            response: ApiResponse
            try:
                try:
                    response = fn(request, *args, **kwargs)
                except Exception as e:
                    if len(e.args) != 2:
                        raise Exception(str(e), 500)
                    raise
            except Exception as e:
                response = ApiResponse(success=False, code=e.args[1], error=e.args[0])
            finally:
                print(response)
                return JsonResponse(asdict(response), status=response.code)

        # Recuperar los docstrings
        new_fn.__doc__ = fn.__doc__
        return new_fn


class APIViewStructure(APIView, metaclass=ApiViewMetaClass):
    """Estructura de las vistas en la API"""

    no_disponible = Exception("endpoint_no_habilitado", 405)

    def post(self) -> ApiResponse:
        """Default endpoint

        Returns
        -------
        ret: `ApiResponse`:
            - success: ``False``
            - error: ``"endpoint no habilitado"``
            - code: 405
        """
        raise self.no_disponible

    def get(self) -> ApiResponse:
        """Default endpoint

        Returns
        -------
        ret: `ApiResponse`:
            - success: ``False``
            - error: ``"endpoint no habilitado"``
            - code: 405
        """
        raise self.no_disponible

    def put(self) -> ApiResponse:
        """Default endpoint

        Returns
        -------
        ret: `ApiResponse`:
            - success: ``False``
            - error: ``"endpoint no habilitado"``
            - code: 405
        """
        raise self.no_disponible

    def delete(self) -> ApiResponse:
        """Default endpoint

        Returns
        -------
        ret: `ApiResponse`:
            - success: ``False``
            - error: ``"endpoint no habilitado"``
            - code: 405
        """
        raise self.no_disponible
