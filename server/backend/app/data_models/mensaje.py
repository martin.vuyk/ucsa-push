from typing import Dict, List, Literal, TypedDict, Union
from typing_extensions import TypeGuard

from app.helpers.try_bool import try_false_type_guard


class MensajeDict(TypedDict):
    emisor: str
    titulo: str
    timestamp: str
    contenido: str
    prioridad: Union[Literal["normal"], Literal["alta"], Literal["emergencia"]]


@try_false_type_guard
def son_mensajes(dict_list: List[Dict]) -> TypeGuard[MensajeDict]:
    def es_mensaje(mensaje):
        return (
            isinstance(mensaje.get("emisor"), str)
            and isinstance(mensaje.get("contenido"), str)
            and mensaje.get("prioridad") in ["normal", "alta", "emergencia"]
        )

    return all(es_mensaje(mensaje) for mensaje in dict_list)
