from typing import Dict, Literal, TypedDict, List, Union
from typing_extensions import TypeGuard

from app.helpers.try_bool import try_false_type_guard


class MateriaUcsaBackendDict(TypedDict):
    uid: str
    uid_profesor: str
    descripcion: str
    dia_clase: int  # 1 = Lunes ... 7 = Domingo
    turno: int  # 1 = mañana, 2 = tarde, 3 = noche
    fecha_vencimiento: str  # ISO datetime


class LoginDict(TypedDict):
    success: bool
    uid: str
    username: str
    celular: str
    lista_materias: List[MateriaUcsaBackendDict]


@try_false_type_guard
def es_ucsa_backend_materia(thing: Dict) -> TypeGuard[MateriaUcsaBackendDict]:
    if isinstance(thing, dict):
        return (
            isinstance(thing.get("uid"), str)
            and isinstance(thing.get("uid_profesor"), str)
            and isinstance(thing.get("dia_clase"), int)
            and isinstance(thing.get("fecha_vencimiento"), str)
            and len(thing.get("fecha_vencimiento")) == 10
        )
