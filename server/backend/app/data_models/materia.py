from typing import Dict, TypedDict
from typing_extensions import TypeGuard

from app.helpers.try_bool import try_false_type_guard


class MateriaDict(TypedDict):
    uid: str
    username_profesor: str
    descripcion: str
    dia_clase: int  # 1 = Lunes ... 7 = Domingo
    turno: int  # 1 = manhana, 2 = tarde, 3 = noche
    fecha_vencimiento: str  # ISO datetime


@try_false_type_guard
def es_materia(thing: Dict) -> TypeGuard[MateriaDict]:
    if isinstance(thing, dict):
        return (
            isinstance(thing.get("uid"), str)
            and isinstance(thing.get("username_profesor"), str)
            and isinstance(thing.get("dia_clase"), int)
            and isinstance(thing.get("turno"), int)
        )
