from django.contrib import admin
from app.models import User, Administrador, ChatRoom, Materia, Mensaje, Profesor

admin.site.register(User)
admin.site.register(Administrador)
admin.site.register(Profesor)
admin.site.register(Materia)
admin.site.register(Mensaje)
admin.site.register(ChatRoom)
