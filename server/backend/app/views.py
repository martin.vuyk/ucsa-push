"""
Endpoints REST
"""
import datetime
import json
from typing import List, Union
from typing_extensions import TypeGuard
from app.data_models.apistructure import APIViewStructure, ApiResponse, ApiViewMetaClass
from app.data_models.login import LoginDict
from app.models import *
from app.mensajeria.hermes import Hermes
from app.data_models.login import MateriaUcsaBackendDict
from app.data_models.mensaje import MensajeDict, son_mensajes
from app.helpers.try_bool import try_false_type_guard
import hashlib
from random import randint


class LoginView(APIViewStructure, metaclass=ApiViewMetaClass):
    """
    Responsable del login
    """

    def post(self, request, *args, **kwargs):
        """Actualiza token y materias del usuario en la base de datos

        Parameters
        ----------
        body: `dict`
            - ``USERNAME``: `str`
            - ``HASHED_PW``: `str`

        Returns
        -------
        ret: `ApiResponse`
            - success: ``True``
            - code: ``201``
            - data: `dict`
                - token: `str`
        """
        try:
            username = request.data.get("USERNAME")
            hashed_pw = request.data.get("HASHED_PW")
            if username == "" or hashed_pw == "":
                raise Exception()
        except Exception:
            raise Exception("input_invalido", 415)
        # password = self.__get_password(username, hashed_pw)
        # login: LoginDict = requests.post(
        #     "ucsa-backend-login-url",
        #     json=json.dumps(dict(usuario=username, password=password)),
        # )
        login = LoginDict(
            success=True,
            uid=username,
            username=username,
            celular="+595971888888",
            lista_materias=[
                MateriaUcsaBackendDict(
                    uid="1",
                    uid_profesor="test_profesor",
                    descripcion="Materia 1",
                    dia_clase=1,
                    turno=3,
                    fecha_vencimiento=(
                        datetime.datetime.now() + datetime.timedelta(days=100)
                    ).replace(tzinfo=datetime.timezone.utc),
                ),
                MateriaUcsaBackendDict(
                    uid="2",
                    uid_profesor="test_profesor",
                    descripcion="Materia 2",
                    dia_clase=2,
                    turno=2,
                    fecha_vencimiento=(
                        datetime.datetime.now() + datetime.timedelta(days=100)
                    ).replace(tzinfo=datetime.timezone.utc),
                ),
                MateriaUcsaBackendDict(
                    uid="3",
                    uid_profesor="test_profesor",
                    descripcion="Materia 3",
                    dia_clase=5,
                    turno=1,
                    fecha_vencimiento=(
                        datetime.datetime.now() + datetime.timedelta(days=100)
                    ).replace(tzinfo=datetime.timezone.utc),
                ),
            ],
        )
        if not login["success"]:
            raise Exception("login_no_valido", 401)
        user = self.__save_user_to_db(login)
        MateriasView.save_lista_materias(user, login["lista_materias"])
        return ApiResponse(success=True, code=201, data={"token": user.token})

    def get(self, request, *args, **kwargs):
        """Verifica las credenciales de un usuario.

        Parameters
        ----------
        headers: `str`
            - USERNAME: `str`
            - TOKEN: `str`

        Returns
        -------
        ret: `ApiResponse`
            - success: ``True``
            - data: `dict`
                - nivel: "usuario" | "profesor" | "administrador"
            - code: ``200``
        """
        user = self.verify_user(request)
        profesor = Profesor.is_stored_in_DB(user.uid)
        admin = Administrador.is_stored_in_DB(user.uid)
        return ApiResponse(
            success="true",
            code=200,
            data=dict(
                nivel="administrador"
                if admin
                else "profesor"
                if profesor
                else "usuario"
            ),
        )

    def __get_password(self, username: str, hashed_pw: str) -> str:
        # TODO: implementar algoo que funcione como este ejemplo:
        # key = os.environ["USER_PW_HASH_KEY"]
        # return rsa.decrypt(hashed_pw, f"{key}:{username}").decode('ascii')
        return hashed_pw

    def __save_user_to_db(self, login: LoginDict):
        fecha_actual = datetime.datetime.now()
        string = f"{fecha_actual}:{login['username']:{randint(1, 99)}}"
        token = hashlib.sha256(string.encode("utf-8")).hexdigest()[:255]
        validez = fecha_actual + datetime.timedelta(days=14.0)
        user = User.objects.update_or_create(
            uid=login["uid"],
            defaults=dict(
                username=login["username"],
                token=token,
                validez_token=validez,
                celular=login["celular"],
            ),
        )[0]
        return user

    @staticmethod
    def verify_user(request) -> User:
        username: str = request.META.get("HTTP_USERNAME")
        token: str = request.META.get("HTTP_TOKEN")
        print(f"username: {username}, token: {token}")
        if username is None or token is None:
            raise Exception("input_invalido", 415)
        if not User.is_stored_in_DB(username):
            raise Exception("usuario_no_encontrado", 404)
        user = User.objects.get(username=username)
        if user.token != token or user.validez_token < datetime.datetime.now(
            tz=datetime.timezone.utc
        ):
            raise Exception("token_invalido", 401)
        return user

    @staticmethod
    def verify_productor(request):
        user = LoginView.verify_user(request)
        productores = [Profesor, Administrador]
        if any(productor.is_stored_in_DB(user.uid) for productor in productores):
            return user
        raise Exception("usuario_no_autorizado", 403)

    @staticmethod
    def verify_administrador(request):
        user = LoginView.verify_user(request)
        if not Administrador.is_stored_in_DB(uid=user.uid):
            raise Exception("usuario_no_autorizado", 403)
        return user


class ChatListView(APIViewStructure, metaclass=ApiViewMetaClass):
    """Ednpoint para listado de chats"""

    def get(self, request, *args, **kwargs):
        """Devuelve la lista de chats de un Usuario

        Parameters
        ----------
        headers: `str`
            - USERNAME: `str`
            - TOKEN: `str`
        """
        user = LoginView.verify_user(request)
        chat_list = []
        for materia in user.suscripciones.all():
            if ChatRoom.is_stored_in_DB(materia):
                chat_list.append(ChatRoom.objects.get(materia=materia).get_chat())
        return ApiResponse(success=True, code=200, data=chat_list)


class ChatRoomView(APIViewStructure, metaclass=ApiViewMetaClass):
    """Enpoint para acceder a informacion de cada chat con su uid"""

    def post(self, request, *args, **kwargs):
        """Crea un ChatRoom en la base de datos

        Parameters
        ----------
        headers: `str`
            - USERNAME: `str`
            - TOKEN: `str`
        body: `dict`
            - "chat": `ChatDict`

        Returns
        -------
        ret: `ApiResponse`
            - success: ``True``
            - code: ``201``
        """
        LoginView.verify_administrador(request)
        uid = kwargs["uid"]
        chat_dict = request.data["chat"]
        if not ChatRoom.es_chat(chat_dict):
            raise Exception("formato_de_chat_incorrecto", 400)
        if not Materia.is_stored_in_DB(uid):
            raise Exception("materia_no_existente", 404)
        materia = Materia.objects.get(uid=uid)
        ChatRoom.objects.create(
            titulo=chat_dict["titulo"],
            last_mensaje_time=chat_dict["lastMessageTime"],
            mensaje_vista=chat_dict["previewMessage"],
            icono=chat_dict["icon"],
            materia=materia,
            mensajes=[],
        )
        return ApiResponse(success=True, code=201)

    def get(self, request, *args, **kwargs):
        """Devuelve un chat

        Parameters
        ----------
        headers: `str`
            - USERNAME: `str`
            - TOKEN: `str`

        Returns
        -------
        ret: `ApiResponse`
            - success: ``True``
            - code: ``200``
            - data: `ChatRoomDict`
        """
        LoginView.verify_user(request)
        uid = kwargs["uid"]
        if not Materia.is_stored_in_DB(uid):
            raise Exception("materia_no_existente", 404)
        materia = Materia.objects.get(uid=uid)
        if not ChatRoom.is_stored_in_DB(materia):
            raise Exception("chatRoom_no_encontrado", 400)
        chat_room = ChatRoom.objects.get(materia=materia).get_chat_room()
        return ApiResponse(success=True, code=200, data=chat_room)

    def put(self, request, *args, **kwargs):
        """Marcar como leido el chat para el user

        Parameters
        ----------
        headers: `str`
            - USERNAME: `str`
            - TOKEN: `str`
        """
        user = LoginView.verify_user(request)
        uid = kwargs["uid"]
        materia = Materia.objects.filter(uid=uid).first()
        if materia is None:
            raise Exception("materia_no_existente", 404)
        if not user.suscripciones.filter(uid=materia.uid).exists():
            raise Exception("usuario_no_suscrito", 404)
        last_mensaje = ProducirView.get_last_mensaje(user, uid)
        previous_last = user.ult_lecturas.filter(materia=materia).first()
        if last_mensaje is not None:
            if previous_last is not None:
                if previous_last.timestamp != last_mensaje.timestamp:
                    user.ult_lecturas.remove(previous_last)
            user.ult_lecturas.add(last_mensaje)

        return ApiResponse(success=True, code=200)


class ProducirView(APIViewStructure, metaclass=ApiViewMetaClass):
    """Endpoint para producir mensajes"""

    def get(self, request, *args, **kwargs):
        """Devuelve los ultimos mensajes producidos (segun id) que el usuario no haya leido

        Parameters
        ----------
        headers: `str`
            - USERNAME: `str`
            - TOKEN: `str`
        query_params: `dict`
            - ids: `list` [`str`]

        Returns
        -------
        ret: `dict`
            - uid_materia: `MensajeDict`
        """
        user = LoginView.verify_user(request)
        try:
            ids = json.loads(request.query_params.get("ids"))
            if (
                ids is None
                or len(ids) == 0
                or not all(isinstance(uid, str) for uid in ids)
            ):
                raise
        except:
            raise Exception("input_invalido", 400)
        mensajes = {}
        for uid in ids:
            last_mensaje = self.get_last_mensaje(user, uid)
            mensajes[uid] = None
            if last_mensaje is not None:
                if not user.ult_lecturas.filter(
                    materia=last_mensaje.materia, timestamp=last_mensaje.timestamp
                ).exists():
                    mensajes[uid] = last_mensaje.get_object_data()

        return ApiResponse(success=True, code=200, data=mensajes)

    def post(self, request, *args, **kwargs):
        """El usuario que tenga autorizacion puede enviar mensajes

        Parameters
        ----------
        headers: `str`
            - USERNAME: `str`
            - TOKEN: `str`
        body: `dict`
            - mensajes: `list` [`MensajeDict`]
            - uid_materias: `list` [`str`]

        Returns
        -------
        ret: `ApiResponse`
            - success: ``True``
            - code: ``201``
        """
        user = LoginView.verify_productor(request)
        uid_materias = request.data.get("uid_materias")
        mensajes = request.data.get("mensajes")
        self.__procesar_mensajes(user, uid_materias, mensajes)
        return ApiResponse(success=True, code=201)

    # TODO: agregar la capacidad de editar el contenido del mensaje hasta 24 horas despues
    # def put(self, request, *args, **kwargs):
    #     pass

    def delete(self, request, *args, **kwargs):
        """Eliminar mensaje de la materia

        Parameters
        ----------
        headers: `str`
            - USERNAME: `str`
            - TOKEN: `str`
        body: `dict`
            - uid_materia: `str`
            - mensaje: `MensajeDict`
        """
        LoginView.verify_administrador(request)
        uid_materia = request.data["uid_materia"]
        mensaje = request.data["mensaje"]
        self.__eliminar_mensaje(uid_materia, mensaje)
        return ApiResponse(success=True, code=200)

    @staticmethod
    def get_last_mensaje(user: User, uid: str) -> Union[Mensaje, None]:
        materia = Materia.objects.filter(uid=uid).first()
        if materia is None:
            raise Exception("materia_no_encontrada", 404)
        if not user.suscripciones.filter(uid=materia.uid).exists():
            raise Exception("suscripcion_no_encontrada", 404)
        chatroom = ChatRoom.objects.filter(materia=materia).first()
        if chatroom is None:
            raise Exception("chatroom_no_existente", 404)
        return chatroom.mensajes.order_by("timestamp").last()

    def __eliminar_mensaje(self, uid_materia, mensaje):
        [uid_materia], [mensaje] = self.__verificar_tipos([uid_materia], [mensaje])
        if not Materia.is_stored_in_DB(uid_materia):
            raise Exception("materia_no_existente", 404)
        materia = Materia.objects.get(uid=uid_materia)
        if not Mensaje.is_stored_in_DB(materia, mensaje["timestamp"]):
            raise Exception("mensaje_no_existente", 404)
        Mensaje.objects.get(materia=materia, timestamp=mensaje["timestamp"]).delete()

    def __procesar_mensajes(self, user: User, uid_materias, mensajes):
        uid_materias, mensajes = self.__verificar_tipos(uid_materias, mensajes)
        unicast, multicast = self.__determinar_modo_mensajeria(uid_materias, mensajes)
        if not unicast and not multicast:
            raise Exception("modo_procesamiento_mensaje_no_soportado", 405)
        if any(not Materia.is_stored_in_DB(uid) for uid in uid_materias):
            raise Exception("suscripcion_no_valida", 404)
        for mensaje in mensajes:
            if len(mensaje) > 255:
                raise Exception("solo_mensajes_menores_a_255_char", 415)
            if mensaje["emisor"] != user.username:
                raise Exception("suplantacion_de_identidad", 403)
            # por alguna razon los de python no dejan que el iso string
            # tenga la Z al final... wtf
            parsed_t = mensaje["timestamp"].split("Z")[0]
            if datetime.datetime.fromisoformat(
                parsed_t
            ) < datetime.datetime.now() - datetime.timedelta(days=1):
                raise Exception("timestamp_mas_antigua_que_1_dia", 405)
        Hermes.enviar_mensajes(user, uid_materias, mensajes, unicast)

    def __determinar_modo_mensajeria(self, uid_materias, mensajes):
        unicast = len(uid_materias) == len(mensajes)
        multicast = len(mensajes) == 1
        return (unicast, multicast)

    def __verificar_tipos(self, topics, mensajes):
        mensajes_validos = self.__verificar_mensajes(mensajes)
        topics_validos = self.__verificar_topics(topics)
        if not mensajes_validos or not topics_validos:
            raise Exception("formato_topics_y_o_mensajes_no_valido", 400)
        return topics, mensajes

    @try_false_type_guard
    def __verificar_lista(self, item) -> TypeGuard[list]:
        return isinstance(item, list) and len(item) > 0

    @try_false_type_guard
    def __verificar_mensajes(self, mensajes) -> TypeGuard[List[MensajeDict]]:
        if self.__verificar_lista(mensajes):
            if all(isinstance(mensaje, dict) for mensaje in mensajes):
                return son_mensajes(mensajes)

    @try_false_type_guard
    def __verificar_topics(self, topics) -> TypeGuard[List[str]]:
        if self.__verificar_lista(topics):
            return all(isinstance(topic, str) for topic in topics)


class MateriasView(APIViewStructure, metaclass=ApiViewMetaClass):
    """Endpoint para Materias"""

    def post(self, request, *args, **kwargs):
        """Crear una materia

        Parameters
        ----------
        headers: `str`
            - USERNAME: `str`
            - TOKEN: `str`
        body: `dict`
            - materia: `MateriaUcsaBackendDict`
        """
        LoginView.verify_administrador(request)
        materia_dict = request.data["materia"]
        if not Materia.es_ucsa_backend_materia(materia_dict):
            raise Exception("sintaxis_materia_invalida", 400)
        MateriasView.get_or_create_materia_profesor(materia_dict)
        return ApiResponse(success=True, code=201)

    def delete(self, request, *args, **kwargs):
        """Elimina el uid especificado en el body del request.

        Parameters
        ----------
        headers: `str`
            - USERNAME: `str`
            - TOKEN: `str`
        body: `dict`
            - uid: `str`

        Notes
        -----
        Si el uid es ``"CLEANUP"``, elimina todas las materias que caducaron
        """
        LoginView.verify_administrador(request)
        uid = request.data.get("uid")
        if uid == "CLEANUP":
            materias_caducadas = Materia.objects.filter(
                fecha_vencimiento__lte=datetime.datetime.now()
            )
            id_eliminadas = [materia.uid for materia in materias_caducadas]
            materias_caducadas.delete()
            return ApiResponse(success=True, code=200, data=id_eliminadas)
        else:
            if not Materia.is_stored_in_DB(uid):
                raise Exception("materia_inexistente", 404)
            Materia.objects.get(uid=uid).delete()
            return ApiResponse(success=True, code=200)

    def get(self, request, *args, **kwargs):
        """Devuelve ids de materias filtradas con parametros especificados

        Parameters
        ----------
        headers: `str`
            - USERNAME: `str`
            - TOKEN: `str`

        query_params: `dict`
            - dias: `list` [`int`]
            - turnos: `list` [`int`]
            - profesores: `list` [`str`]

        Returns
        -------
        ret: `ApiResponse`
            - success: ``True``
            - data: `MateriaDict`
            - code: ``200``
        """
        LoginView.verify_administrador(request)
        correcto, (dias, turnos, profesores) = self.verificar_input(request)
        if not correcto:
            raise Exception("filtros_invalidos", 400)
        filtrado = []

        def filtrar(campo, elementos):
            print(campo, elementos)
            lista = [Materia.objects.filter(**{campo: valor}) for valor in elementos]
            return lista[0].union(*lista[1:])

        if len(profesores) > 0:
            for profesor in profesores:
                profe = Profesor.objects.filter(uid=profesor)
                if not profe.exists():
                    raise Exception("profesor_no_encontrado", 404)
                filtrado.append(filtrar("profesor", profe))
        if len(dias) > 0:
            filtrado.append(filtrar("dia", dias))
        if len(turnos) > 0:
            filtrado.append(filtrar("turno", turnos))
        print("getting data")
        if len(filtrado) > 0:
            materias = filtrado[0].intersection(*filtrado[1:])
        else:
            materias = Materia.objects
        data = [materia.get_object_data() for materia in materias.all()]
        return ApiResponse(success=True, code=200, data=data)

    def verificar_input(self, request):
        try:
            args = [
                (json.loads(request.query_params.get("dias")), int),
                (json.loads(request.query_params.get("turnos")), int),
                (json.loads(request.query_params.get("profesores")), str),
            ]

            def verify(obj, tipo):
                return isinstance(obj, tipo)

            for arg in args:
                if arg[0] is None:
                    raise Exception("filtro_null_debe_ser_[]", 400)
                if not verify(arg[0], list):
                    print(f"no es lista: {arg[0]}, es: {type(arg[0])}")
                    return False, (None, None, None)
                if len(arg[0]) == 0:
                    continue
                if not all(verify(item, arg[1]) for item in arg[0]):
                    print(f"no todos los items son {arg[1]} en {arg[0]}")
                    return False, (None, None, None)
            return True, (args[0][0], args[1][0], args[2][0])
        except:
            return False, (None, None, None)

    @staticmethod
    def save_lista_materias(user: User, materias_list: List[MateriaUcsaBackendDict]):
        """Actualiza listado de suscripciones del usuario\n
        Crea Profesores si no existen, Crea Materias si no existen.\n
        No sobreescribe ni duplica nada
        """
        try:
            for materia in materias_list:
                materia_en_db = MateriasView.get_or_create_materia_profesor(materia)
                user.suscripciones.add(materia_en_db)
            user.save()
        except Exception as e:
            print(e)
            raise Exception("suscripcion_no_valida", 400)

    @staticmethod
    def get_or_create_materia_profesor(materia: MateriaUcsaBackendDict):
        uid_profesor = materia["uid_profesor"]
        profesor = None
        if uid_profesor != "":
            profesor = Profesor.objects.get_or_create(uid=uid_profesor)[0]
        materia_db = Materia.objects.update_or_create(
            uid=materia["uid"],
            defaults=dict(
                profesor=profesor,
                dia=materia["dia_clase"],
                turno=materia["turno"],
                fecha_vencimiento=materia["fecha_vencimiento"],
                descripcion=materia["descripcion"],
            ),
        )[0]
        chatRoom, recien_creado = ChatRoom.objects.get_or_create(
            materia=materia_db,
            defaults=dict(
                titulo=materia_db.descripcion,
                last_mensaje_time=datetime.datetime.now(),
                mensaje_vista="",
                icono="mdi-school",
            ),
        )
        if recien_creado:
            mensajes = Mensaje.objects.filter(materia=materia_db)
            if mensajes.exists():
                for mensaje in mensajes.all():
                    chatRoom.mensajes.add(mensaje)
        return materia_db
