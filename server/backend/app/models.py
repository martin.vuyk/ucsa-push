import datetime
from django.db import models
from typing_extensions import TypeGuard
from app.data_models.apistructure import APIModelStructure
from app.data_models.chat import (
    ChatDict,
    ChatRoomDict,
    es_chat,
    es_chat_room,
)
from app.data_models.login import (
    MateriaUcsaBackendDict,
    es_ucsa_backend_materia,
)
from app.data_models.materia import MateriaDict, es_materia
from app.data_models.mensaje import MensajeDict


class Administrador(APIModelStructure, models.Model):
    uid = models.CharField(max_length=255, unique=True, null=False, blank=False)

    def get_object_data(self):
        return {"uid": self.uid}

    @classmethod
    def is_stored_in_DB(cls, uid):
        return APIModelStructure.is_stored_in_DB(cls, field="uid", value=uid)


class Profesor(APIModelStructure, models.Model):
    uid = models.CharField(max_length=255, unique=True, null=False, blank=False)

    def get_object_data(self):
        return {"uid": self.uid}

    @classmethod
    def is_stored_in_DB(cls, uid):
        return APIModelStructure.is_stored_in_DB(cls, field="uid", value=uid)


class Materia(APIModelStructure, models.Model):
    uid = models.CharField(max_length=255, unique=True, null=False, blank=False)
    descripcion = models.CharField(max_length=255, null=True, blank=True)
    profesor = models.ForeignKey(Profesor, models.PROTECT, null=True, blank=True)
    dia = models.PositiveIntegerField(blank=False)
    turno = models.PositiveIntegerField(blank=False)
    fecha_vencimiento = models.DateTimeField(null=False, blank=False)

    def get_object_data(self):
        profesor = User.objects.filter(uid=self.profesor.uid)
        if profesor.exists():
            username = profesor.get().username
        else:
            username = ""
        return MateriaDict(
            uid=self.uid,
            username_profesor=username,
            descripcion=self.descripcion,
            dia_clase=self.dia,
            turno=self.turno,
            fecha_vencimiento=self.fecha_vencimiento.isoformat(),
        )

    def es_materia(thing) -> TypeGuard[MateriaDict]:
        return es_materia(thing)

    def es_ucsa_backend_materia(thing) -> TypeGuard[MateriaUcsaBackendDict]:
        return es_ucsa_backend_materia(thing)

    @classmethod
    def is_stored_in_DB(cls, uid):
        return APIModelStructure.is_stored_in_DB(cls, field="uid", value=uid)


class User(APIModelStructure, models.Model):
    uid = models.CharField(max_length=255, unique=True, null=False, blank=False)
    username = models.CharField(max_length=255, unique=True, null=False, blank=False)
    celular = models.CharField(max_length=255, unique=False, null=True, blank=True)
    token = models.CharField(max_length=255, unique=True, null=False, blank=False)
    validez_token = models.DateTimeField(null=False, blank=False)
    suscripciones = models.ManyToManyField(Materia, blank=True)
    ult_lecturas = models.ManyToManyField("Mensaje", blank=True)

    def get_object_data(self):
        return {
            "uid": self.uid,
            "username": self.username,
            "celular": self.celular,
            "token": self.token,
            "suscripciones": [
                materia.get_object_data() for materia in self.suscripciones.all()
            ],
        }

    @classmethod
    def is_stored_in_DB(cls, username):
        return APIModelStructure.is_stored_in_DB(cls, field="username", value=username)


class Mensaje(APIModelStructure, models.Model):
    emisor = models.ForeignKey(User, on_delete=models.PROTECT)
    titulo = models.CharField(max_length=255, unique=False, null=False, blank=False)
    timestamp = models.DateTimeField(null=False, blank=False)
    contenido = models.CharField(max_length=255, null=False, blank=False)
    prioridad = models.CharField(max_length=255, null=False, blank=False)
    materia = models.ForeignKey(Materia, on_delete=models.CASCADE)

    def get_object_data(self):
        return MensajeDict(
            emisor=self.emisor.username,
            titulo=self.titulo,
            timestamp=self.timestamp.isoformat(),
            contenido=self.contenido,
            prioridad=self.prioridad,
        )

    @classmethod
    def is_stored_in_DB(cls, materia: Materia, timestamp: str):
        return APIModelStructure.is_stored_in_DB(
            cls,
            field="timestamp",
            value=datetime.datetime.fromisoformat(timestamp.split("Z")[0]),
        ) and APIModelStructure.is_stored_in_DB(cls, field="materia", value=materia)


class ChatRoom(APIModelStructure, models.Model):
    titulo = models.CharField(max_length=255, unique=False, null=False, blank=False)
    last_mensaje_time = models.DateTimeField(null=False, blank=False)
    mensaje_vista = models.CharField(max_length=255, null=False, blank=False)
    icono = models.CharField(max_length=255, null=False, blank=False)
    materia = models.OneToOneField(Materia, models.CASCADE)
    mensajes = models.ManyToManyField(Mensaje, blank=True)

    def get_object_data(self):
        return {"uid": self.materia.uid}

    def get_chat_room(self):
        return ChatRoomDict(
            chatId=self.materia.uid,
            titulo=self.titulo,
            descripcion=self.materia.descripcion,
            mensajes=[mensaje.get_object_data() for mensaje in self.mensajes.all()],
        )

    def get_chat(self):
        return ChatDict(
            id=self.materia.uid,
            icon=self.icono,
            previewMessage=self.mensaje_vista,
            titulo=self.titulo,
            descripcion=self.materia.descripcion,
            lastMessageTime=self.last_mensaje_time.isoformat(),
        )

    def update_from_chat(self, chat: ChatDict):
        self.descripcion = chat["descripcion"]
        self.titulo = chat["titulo"]
        self.mensaje_vista = chat["previewMessage"]
        self.icono = chat["icon"]

    @staticmethod
    def es_chat(thing) -> TypeGuard[ChatDict]:
        return es_chat(thing)

    @staticmethod
    def es_chat_room(thing) -> TypeGuard[ChatRoomDict]:
        return es_chat_room(thing)

    @classmethod
    def is_stored_in_DB(cls, materia):
        return APIModelStructure.is_stored_in_DB(cls, field="materia", value=materia)
