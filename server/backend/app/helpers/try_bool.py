from typing import Callable, Optional, TypeVar
from typing_extensions import TypeGuard

T = TypeVar("T")


def try_false_type_guard(
    fn: Callable[..., TypeGuard[T]]
) -> Callable[..., TypeGuard[T]]:
    """un wrapper para que la funcion se ocupe del caso positivo del TypeGuard.\n
    si la funcion es void o hay un error en la ejecuccion, se devuelve False para el TypeGuard"""

    def new_fn(*args, **kwargs) -> bool:
        try:
            ret: Optional[bool] = fn(*args, **kwargs)
            if ret is None:
                return False
            return ret
        except:
            return False

    # Recuperar los docstrings
    new_fn.__doc__ = fn.__doc__
    return new_fn
