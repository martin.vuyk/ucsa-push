from .views import *
from django.urls import path
from drf_spectacular.views import (
    SpectacularAPIView,
    SpectacularRedocView,
    SpectacularSwaggerView,
)

urlpatterns = [
    path("schema/", SpectacularAPIView.as_view(), name="schema"),
    path(
        "docs/swagger",
        SpectacularSwaggerView.as_view(url_name="schema"),
        name="swagger",
    ),
    path("login", LoginView.as_view(), name="login"),
    path("producir", ProducirView.as_view(), name="producir"),
    path("materias", MateriasView.as_view(), name="materias"),
    path("chatlist", ChatListView.as_view(), name="chatlist"),
    path("chatroom/<uid>", ChatRoomView.as_view(), name="chatroom"),
]
