# API desarrollada para administrar suscripciones y mensajeria de dichas suscripciones, con un modelo de Administrador, Profesor, y Usuario.

Usuarios comunes pueden consumir su lista de suscripciones (Materias) y sus mensajes.<br />
Administradores y Profesores pueden producir mensajes para cada Materia.<br />
Administradores pueden editar y eliminar materias y mensajes.

### Documentacion Swagger y Redoc una vez levantado el servidor en: base_url/api/${version_api:-v1}/docs/swagger

### Documentacion del Backend como modulo de Python, para ver toda la estructura interna de la API, ejecutando ./scripts/generar_documentacion (.sh o .bat dependiendo del OS) luego de haber ejecutado ./scripts/levantar_entorno_dev
