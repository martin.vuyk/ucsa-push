# habilitar el ejecutable ejecutando antes: chmod +x ./levantar_entorno_dev.sh
#!/bin/bash
virtualenv venv --distribute
source venv/bin/activate
python -m pip install --upgrade pip
pip install -r requirements.txt
